from django.contrib import admin

from web.models import MedicalService


class MedicalServiceAdmin(admin.ModelAdmin):
    list_display = 'id', 'name', 'price', 'category_id', 'doctor_id'
    search_fields = 'id', 'name'
    list_filter = ('price', 'name')


admin.site.register(MedicalService, MedicalServiceAdmin)
