from django.contrib import admin
from django.urls import path

from web.views import main_view, registration_view, auth_view, logout_view, category_get_service_view, \
    create_reception_view, \
    save_reception_view, get_my_receptions_view, delete_reception_view, analytics_view

urlpatterns = [
    path("", main_view, name="main"),
    path("reg/", registration_view, name="reg"),
    path("auth/", auth_view, name="auth"),
    path("logout/", logout_view, name="logout"),
    path("category/<int:id>", category_get_service_view, name="services"),
    path("reception/get_times/<int:id>", create_reception_view, name="rec_add"),
    path("reception/add/<int:id>/<str:time>", save_reception_view, name="rec_add2"),
    path("reception/my", get_my_receptions_view, name="my_res"),
    path("reception/delete/<int:id>", delete_reception_view, name="delete_res"),
    path("analytics/", analytics_view, name="analytics")
]
