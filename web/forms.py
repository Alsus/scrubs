from django import forms
from django.contrib.auth import get_user_model

from web.models import Category, Reception, Doctor, BusyTime

User = get_user_model()


class RegistrationForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    password2 = forms.CharField(widget=forms.PasswordInput())

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data['password'] != cleaned_data['password2']:
            self.add_error("password", "Пароли не совпадают")
        return cleaned_data

    class Meta:
        model = User
        fields = ("email", "username", "password", "password2")


class AuthForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = {'name'}

    def save(self, commit=True):
        return super().save(commit)


class MedicalServiceFilterForm(forms.Form):
    search = forms.CharField(widget=forms.TextInput(attrs={"placeholder": "Поиск"}), required=False)
    price = forms.CharField(widget=forms.TextInput(attrs={"placeholder": "Цена"}), required=False)
