from django.core.paginator import Paginator
from django.db.models import Count, Max, Min
from django.db.models.expressions import F
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import get_user_model, authenticate, login, logout
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

from web.forms import AuthForm, RegistrationForm, CategoryForm, MedicalServiceFilterForm
from web.models import MedicalService, Category, BusyTime, Reception

User = get_user_model()

free_time = ['9:00 - 9:20', '9:25 - 9:45', '9:50 - 10:10', '10:15 - 10:35',
             '10:40 - 11:00', '11:05 - 11:25', '11:30 - 11:50', '11:55- 12:15',
             '12:20 - 12:40', '12:45 - 13:05', '14:30 - 14:50', '14:55 - 15:15',
             '15:20 - 15:40', '15:45 - 16:05', '16:10 - 16:30', '16:35 - 16:55']


def main_view(request):
    categories = Category.objects.all()
    return render(request, "web/main.html", {
        'categories': categories
    })


def registration_view(request):
    form = RegistrationForm()
    is_success = False
    if request.method == 'POST':
        form = RegistrationForm(data=request.POST)
        if form.is_valid():
            user = User(
                username=form.cleaned_data['username'],
                email=form.cleaned_data['email'],
            )
            user.set_password(form.cleaned_data['password'])
            user.save()
            is_success = True
    return render(request, "web/reg.html", {
        "form": form, "is_success": is_success
    })


def auth_view(request):
    form = AuthForm()
    if request.method == 'POST':
        form = AuthForm(data=request.POST)
        if form.is_valid():
            user = authenticate(**form.cleaned_data)
            if user is None:
                form.add_error(None, "Введены неверные данные")
            else:
                login(request, user)
                return redirect("main")
    return render(request, "web/auth.html", {"form": form})


def logout_view(request):
    logout(request)
    return redirect("main")


def category_get_service_view(request, id):
    services = MedicalService.objects.filter(category_id=id)

    filter_form = MedicalServiceFilterForm(request.GET)
    filter_form.is_valid()
    filters = filter_form.cleaned_data

    if filters['search']:
        services = services.filter(name__icontains=filters['search'])

    if filters['price']:
        services = [i for i in services if i.price[:len(i.price)-1] <= filters['price']]

    page_number = request.GET.get("page", 1)
    paginator = Paginator(services, per_page=4)

    return render(request, "web/services.html",
                  {"services": paginator.get_page(page_number),
                   "filter_form": filter_form,
                   "total_count": len(services)})


@login_required
def create_reception_view(request, id):
    busy_time = BusyTime.objects.filter(service_id=id)
    res = [x for x in free_time if all(y.time not in x for y in busy_time)]
    return render(request, "web/reception.html", {"free_time": res, "service_id": id})


@login_required
def save_reception_view(request, id, time):
    service = get_object_or_404(MedicalService, id=id)
    doctor = service.doctor
    reception = Reception(
        service=service,
        doctor=doctor,
        user=request.user,
        time=time
    )
    busy_time = BusyTime(
        time=time,
        service=service
    )
    reception.save()
    busy_time.save()
    return redirect('main')


@login_required
def get_my_receptions_view(request):
    receptions = Reception.objects.filter(user_id=request.user.id)
    receptions = receptions.prefetch_related("service").select_related("doctor")
    return render(request, "web/my_res.html", {"receptions": receptions})


@login_required
def delete_reception_view(request, id):
    reception = get_object_or_404(Reception, id=id)
    busy_time = BusyTime.objects.get(service_id=reception.service.id, time=reception.time)
    busy_time.delete()
    reception.delete()
    return redirect('my_res')


def analytics_view(request):
    l = MedicalService.objects.filter()
    for i in l:
        i.price = i.price[:-1]

    overall_stat = l.aggregate(
        count=Count("id"),
        max_price=Max("price"),
        min_price=Min("price")
    )
    print("AAAAA", overall_stat)
    return render(request, "web/analytics.html", {"overall_stat": overall_stat})
