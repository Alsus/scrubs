from django.db import models

from django.contrib.auth import get_user_model

User = get_user_model()


class Category(models.Model):
    name = models.CharField(max_length=300)


class Doctor(models.Model):
    full_name = models.CharField(max_length=300)
    stage = models.CharField(max_length=50)


class MedicalService(models.Model):
    name = models.CharField(max_length=255, verbose_name="Название медицинской услуги ")
    price = models.CharField(max_length=255, verbose_name="Цена")
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, verbose_name="Категория")
    doctor = models.ForeignKey(Doctor, on_delete=models.SET_NULL, null=True, verbose_name="Врач")

    class Meta:
        verbose_name = 'услуга'
        verbose_name_plural = 'медицинские услуги'


class Reception(models.Model):
    time = models.CharField(max_length=100, verbose_name="Время записи")
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    service = models.ForeignKey(MedicalService, on_delete=models.CASCADE, verbose_name="Название медицинской услуги")
    doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE, verbose_name="Врач")


class BusyTime(models.Model):
    time = models.CharField(max_length=100)
    service = models.ForeignKey(MedicalService, on_delete=models.CASCADE)


